import unittest
import survey


class ReverseSurveyTest(unittest.TestCase):

    def setUp(self):
        self.dataset = survey.SurveyDataSet('simple_sample.csv')

    def test_conditionals(self):
        self.assertEqual(
            self.dataset.get_question("Do you have pets?").get_conditionals(),
            []
        )
        self.assertEqual(
            self.dataset.get_question("What kinds of pets do you have?").get_conditionals(),
            [{'where_response_equals': 'Yes', 'determined_by':
              self.dataset.get_question("Do you have pets?")}]
        )
        self.assertEqual(
            self.dataset.get_question("Where do you shop for pet food?").get_conditionals(),
            [{'where_response_equals': 'Yes', 'determined_by':
              self.dataset.get_question("Do you have pets?")}]
        )
        self.assertEqual(
            self.dataset.get_question("Do you visit dog parks?").get_conditionals(),
            [{'where_response_equals': 'Dog', 'determined_by':
              self.dataset.get_question("What kinds of pets do you have?")}]
        )

if __name__ == '__main__':
    unittest.main()
