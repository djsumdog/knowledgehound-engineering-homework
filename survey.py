"""Template for KnowledgeHound interview homework

Please follow the instructions in the README.md file.

"""
import csv
from collections import OrderedDict, defaultdict
from functools import reduce


class SurveyDataSet(object):
    """A single survey's data set.

    Initialized with a single argument, the filename of the CSV data set.

    """
    def __init__(self, dataset_filename):
        # open the data file and create the question objects
        self.questions = []
        with open(dataset_filename, 'rU') as csv_file:
            reader = csv.reader(csv_file)
            variable_names = next(reader)

            data = OrderedDict((name, []) for name in variable_names)
            for line in reader:
                for (cell, name) in zip(line, variable_names):
                    data_point = cell.strip()
                    if data_point == "":
                        data_point = None
                    data[name].append(data_point)

            for (name, column) in data.items():
                self.questions.append(
                    Question(
                        name=name,
                        data=column,
                        parent_dataset=self,
                    )
                )

    def get_question(self, question_label):
        """Return the question matching the label.

        Raises AttributeError if no matching question exists.

        """
        for question in self.questions:
            if question.name == question_label:
                return question

        raise LookupError(
            'no question with label "{0}" in data set'.format(question_label)
        )


class Question(object):
    """A single question.

    Initialized with name, list of responses, reference to its parent study.

    """
    def __init__(self, name, data, parent_dataset):
        self.name = name
        self.data = data
        self.parent_dataset = parent_dataset

    def __repr__(self):
        return "<{}: {}>".format(type(self).__name__, self.name)

    @property
    def sample_size(self):
        """
        Returns an integer representing the number of responses to this
        question
        """
        return sum(1 for item in self.data if item is not None)

    @property
    def histogram(self):
        """
        returns a dictionary whose keys are strings, the possible responses to
        this question and whose values are integers, the count of respondents
        who gave that answer.

        Does not indicate respondents who didn't answer the question.

        """
        histogram = defaultdict(int)
        for data_point in self.data:
            if data_point is None:
                # ignore people who didn't respond
                pass
            else:
                histogram[data_point] += 1
        return histogram

    def __filter_comma_uniques(self, list):
        """
        Filter for unique elements in comma seperated values in a list.
        Example: ['A,B,C', 'A,F', 'L,A'] -> ['A']
        """
        ll = [i.split(',') for i in list]
        return reduce(lambda x, y: set(x).intersection(set(y)), ll)

    def get_conditionals(self):
        """
        returns a list of all conditionals which might have been used to
        determine which respondents saw this question.  Each element in the
        list is a dict in the format:
            {
                "determined_by": ..., # instance of question
                "where_response_equals": ..., # response as string
            }

        Example:
            "Do you have a dog?" is only asked to respondents who answered
            "Yes" to "Do you have pets?".  Calling <Question: "Do you have a
            dog?">'s conditionals method yields:
            [
                {
                    "determined_by": <Question: Do you have pets?>
                    "where_response_equals": "Yes"
                }
            ]
        """
        # If you have no blank answers, you have no dependencies
        if None not in self.data:
            return []

        # Search limits
        qnum = self.parent_dataset.questions.index(self)
        questions = self.parent_dataset.questions[:qnum]

        nones = [i for i, x in enumerate(self.data) if x is None]
        filled = [i for i, x in enumerate(self.data) if x is not None]

        for q in questions[::-1]:

            # Requirement #2 (none in sample set)
            # Ignore questions with only one answer
            if len(set(q.data)) > 1:

                # Potential dependent answers
                matched = self.__filter_comma_uniques(
                  [q.data[x] for x in filled])

                if len(set(matched)) == 1:
                    # Make sure they don't occure elsewhere
                    unmatched = [q.data[x] for x in nones]
                    m = matched.pop()  # Only one element

                    if m[0] not in unmatched:
                        return [{'determined_by': q, 'where_response_equals': m}]
