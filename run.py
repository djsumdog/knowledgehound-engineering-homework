#!/usr/bin/env python

import survey

dataset = survey.SurveyDataSet('simple_sample.csv')
for d in dataset.questions:
    print(d.name)
    print(d.get_conditionals())
    print('-----')
